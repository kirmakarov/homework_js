function buttonClick(){
	var x1 = document.getElementById('x1').value;
	var x2 = document.getElementById('x2').value;
	var radio_sel = document.getElementsByName('action_sel');
	var select_action = '';
	var resultDiv = document.getElementById('result');
	
	if (x1 == "" || x2 == ""){
		alert("Поля x1 и x2 должны быть заполнены");
	}
	else {
		x1 = parseInt(x1);
		x2 = parseInt(x2);
	}

	if (x1 > x2){
		var tmp = x1;
		x1 = x2;
		x2 = tmp;
	}
	
	for (var i = 0; i < radio_sel.length; i++) {
		var input = radio_sel[i];
		if (input.checked == true){
			select_action = input.value;
		};
	}
		
	if (Number.isNaN(x1) || Number.isNaN(x2)){
		alert("В полях присутствуют не числовые символы");
	} else {
		if (select_action == "addition"){
			var sum = 0;
			for(var i=x1; i<=x2; i++){
				sum = sum + i;
			}
			resultDiv.innerHTML = "Сумма всех чисел от x1 до x2 = " + sum;
		} else if (select_action == "miltipl"){
			var fultipl = 1;
			for(var i = x1; i <= x2; i++){
				fultipl = fultipl * i;
			}
			resultDiv.innerHTML = "Произведение всех чисел от x1 до x2 = " + fultipl;
		} else if (select_action == "prime_num"){
			
			if (x2 < 2){
				resultDiv.innerHTML = "В указанном диапазоне простых чисел нет";
			} else if (x2 == 2){
				resultDiv.innerHTML = "В указанном диапазоне присутствует одно простое число - 2";
			} else {
				var arr_prime_num = [2];
				for (var i = 3; i <= x2; i++){
					is_prime_num = true;
					for (var j = 0; j < arr_prime_num.length; j++){
						if (i % arr_prime_num[j] == 0){
							is_prime_num = false;
							break;
						}
					}
					if (is_prime_num == true){
						arr_prime_num.push(i);
					}
				}
				while (arr_prime_num[0] < x1){
					arr_prime_num.shift();
					
				}
				resultDiv.innerHTML = "В указанном диапазоне присутствуют простые числа: " + arr_prime_num;
			}
		}
		else {
			alert("Похоже случился баг");
		}
	}
	
};

function clearForm(){
	document.getElementById('x1').value = '';
	document.getElementById('x2').value = '';	
};